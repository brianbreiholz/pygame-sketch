#!/usr/bin/env python3
from Sketch import Sketch
sketch = Sketch(1800, 1200)


# setup is called once at the start
# the sk parameter is a reference to the sketch class
def setup(sk):
    print("hello from setup")
    pass


# draw is called repeatedly until the user leaves
def draw(sk):
    sk.background((255, 255, 255))

    pos = [sk.width / 2, sk.height / 2]
    size = (100, 100)

    sk.fill((255, 0, 255))
    sk.rect(pos, size)


# ------------------------
# run passes both functions to pygame and starts the sketch
sketch.run(setup, draw)






# --------------------------------------------

