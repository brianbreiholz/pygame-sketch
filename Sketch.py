import pygame

"""
The Sketch class is a wrapper around pygame
that mimics p5 syntax and functionality.
The user has to create an instance and can 
then define a custom setup and draw function 
which have to be passed to pygame via sketch_instance.run(setup, draw).
Both setup and draw have access to Sketch via a function parameter or
through the sketch instance itself. 
"""

class Sketch:

    def __init__(self, width=800, height=600, caption="PyGame Sketch"):
        self.canvas = None
        self.clock = None
        self.width = width
        self.height = height
        self.screen_size = None
        self.caption = caption
        self.pg = pygame
        self.loop = None
        self.fps = 60
        self.fill_color = (0, 0, 0)
        self.background_color = (255, 255, 255)
        self.stroke_width = 0
    # end of constructor

    # clean up is called automatically when the user exits
    def __clean_up(self):
        self.pg.quit()
        quit()

    # updates the screen after the user has drawn to it
    def __render(self):
        self.pg.display.update()
        self.clock.tick()

    # pygame and custom sketch initialization
    def __setup(self, my_setup):
        self.pg.init()
        self.screen_size = (self.width, self.height)
        self.canvas = self.pg.display.set_mode(self.screen_size)
        self.pg.display.set_caption(self.caption)
        self.clock = self.pg.time.Clock()
        self.loop = True
        my_setup(self)

    # check if the user quits the application
    def __check_for_quit(self):
        for event in self.pg.event.get():
            if event.type == self.pg.QUIT:
                self.loop = False

            try:  # check if the escape button was pressed
                if event.key == 27:
                    self.loop = False
                pass
            except AttributeError:
                pass

    # pygame and custom draw loop
    def __draw(self, my_draw):
        while self.loop:
            self.__check_for_quit()
            # my own code that should be executed every frame
            my_draw(self)
            # update screen
            self.__render()

        # ------------------------------
        # clean up after rendering loop
        self.__clean_up()

    # ========================================================
    # user functions -----------------------------------------
    # ========================================================

    # method for the user to pass setup and draw functions
    def run(self, my_setup, my_draw):
        self.__setup(my_setup)
        self.__draw(my_draw)

    # ----------------------------------
    # drawing methods (similar to p5)

    def fill(self, color):
        self.fill_color = color

    def stroke_weight(self, stroke_weight):
        self.stroke_width = stroke_weight
        pass

    def background(self, color):
        self.background_color = color
        self.canvas.fill(self.background_color)

    def rect(self, pos, size):
        self.pg.draw.rect(self.canvas, self.fill_color, (pos, size), self.stroke_width)
        pass

    def circle(self, center, radius):
        self.pg.draw.circle(self.canvas, self.fill_color, center, radius, self.stroke_width)
        pass

    def ellipse(self, pos, size):
        bounding_rect = self.pg.Rect((pos, size))
        self.pg.draw.ellipse(self.canvas, self.fill_color, bounding_rect, self.stroke_width)
        pass

# end of class

